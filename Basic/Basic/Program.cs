﻿using System;

namespace Basic
{
    class Program
    {
        enum userChoice
        {
            GradesFunctions,
            PrintNumbersAndFizzBuzz,
            CheckFirstNumberDivideAllNumbers
        }
        static void Main(string[] args)
        {
            bool keepChecking = true;
            while (keepChecking)
            {
                Console.WriteLine("please enter the the number of function you want to check\r\n" +
                "1) Grades Functions\r\n" +
                "2) Print Numbers And FizzBuzz\r\n" +
                "3) Check First Number Divide All Numbers\r\n" +
                "4) exit");

                int functionToCheck = int.Parse(Console.ReadLine());
                switch (functionToCheck)
                {
                    case 1:
                        GradesFunctions();
                        break;
                    case 2:
                        PrintNumbersAndFizzBuzz();
                        break;
                    case 3:
                        CheckFirstNumberDivideFollowingNumbers();
                        break;
                    case 4:
                        keepChecking = false;
                        break;
                    default:
                        Console.WriteLine("illegal choice, pleas choose again");
                        break;
                }
            }
        }

        static void GradesFunctions()
        {
            int numOfGrades = 0;
            double avg = 0;
            int highest = int.MinValue;
            int lowest = int.MaxValue;
            int NumOfFailures = 0;
            int userGradeInput = -2;

            Console.WriteLine("please enter a grade or -1 if you want to stop");

            try
            {
                userGradeInput = int.Parse(Console.ReadLine());
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            while(userGradeInput != -1)
            {
                numOfGrades += 1;
                if(userGradeInput > highest)
                {
                    highest = userGradeInput;
                }
                if(userGradeInput < lowest)
                {
                    lowest = userGradeInput;
                }
                if(userGradeInput < 60)
                {
                    NumOfFailures += 1;
                }
                avg = (avg * (numOfGrades - 1) + userGradeInput) / numOfGrades;
                Console.WriteLine("please enter a grade or -1 if you want to stop");
                try
                {
                    userGradeInput = int.Parse(Console.ReadLine());
                }catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            if (numOfGrades == 0)
            {
                Console.WriteLine("you didn't enter grades");
            }
            else
            {
                Console.WriteLine($"The average of your grades is {avg}\r\n" +
                $"The highest grade is {highest}\r\n" +
                $"The lowest grade is {lowest}\r\n" +
                $"Number of failures is {NumOfFailures}\r\n");
            }
        }

        static void PrintNumbersAndFizzBuzz()
        {
            for(int i = 1; i <= 1000; i++)
            {
                if(i%3==0 && i % 5 == 0)
                {
                    Console.WriteLine("FizzBuzz");
                }
                else if(i % 3 == 0){
                    Console.WriteLine("Fizz");
                }
                else if (i % 5 == 0)
                {
                    Console.WriteLine("Buzz");
                }
                else
                {
                    Console.WriteLine(i);
                }
            }
        }

        static void CheckFirstNumberDivideFollowingNumbers()
        {
            bool isDivisor = true;
            Console.WriteLine("please enter the dividor number");
            int divisor = int.Parse(Console.ReadLine());
            Console.WriteLine("please enter a number to check division or -1 if you want to stop");
            int userNumberToCheck = int.Parse(Console.ReadLine());

            while (userNumberToCheck != -1)
            {
                isDivisor = isDivisor && userNumberToCheck % divisor == 0;
                Console.WriteLine("please enter a number to check division or -1 if you want to stop");
                userNumberToCheck = int.Parse(Console.ReadLine());
            }
            Console.WriteLine(isDivisor);

        }
    }
}
